enable_language (C)
include(CheckCSourceCompiles)

CHECK_C_SOURCE_COMPILES("
// Below dot must fail syntax validation
.
  int main(void) { return 0; }
  "
  SHOULD_FAIL)

if(NOT SHOULD_FAIL)
  message(SEND_ERROR "Test fail for invalid source syntax")
endif()
